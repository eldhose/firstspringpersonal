package com.project.firstspring;

import java.lang.reflect.Method;

import org.springframework.aop.MethodBeforeAdvice;

public class BeforeAdvisor implements MethodBeforeAdvice {

	@Override
	public void before(Method method, Object[] args, Object target)
			throws Throwable {
		System.out.println("Additional Concern before actual logic");

		System.out.println("The name of the method intersepted " +method.getName() +"\nModifiers " +method.getModifiers());
		System.out.println("Argument Info");
		for(Object arg: args){
			System.out.println(arg);
		}
		System.out.println(target);
		System.out.println(target.getClass().getName());
	}
	

}
