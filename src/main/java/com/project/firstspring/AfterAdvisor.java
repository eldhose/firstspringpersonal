package com.project.firstspring;
import org.springframework.aop.AfterReturningAdvice;

import java.lang.reflect.Method;

/**
 * Created by ldo on 16/11/14.
 */

public class AfterAdvisor implements AfterReturningAdvice{

    @Override
    public void afterReturning(Object returnValue, Method method, Object[] args, Object target) throws Throwable {

        System.out.println("After the method return :)");

    }
}
