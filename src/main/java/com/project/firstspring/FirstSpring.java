package com.project.firstspring;

import org.springframework.context.ApplicationContext;
import org.springframework.context.support.ClassPathXmlApplicationContext;

class FirstSpring{
	public static void main(String[] arg){
        
		Hello hello = new Hello();
        ApplicationContext applicationContext =
        		new ClassPathXmlApplicationContext("applicationContext.xml");
		System.out.println("Just before interceptor");
		A a = applicationContext.getBean("proxy", A.class);
		a.printLogic();
	}
}
